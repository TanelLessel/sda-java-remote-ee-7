package ee.sda.seven.dataandtime;


import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class LocaltimeExample {

    public static void main(String[] args) {

        LocalTime timeNow = LocalTime.now();

        //10:56:25.208
        // HH:mm:ss - pattern
        System.out.println("Now time is " +timeNow
                .format(DateTimeFormatter.ISO_TIME));

        System.out.println("Our lesson will finish today at: " +timeNow
                .plusHours(1)
                .plusMinutes(10));

        LocalDate dateToday = LocalDate.now();

        // TODO 2: Please convert current date to dd/mm/yyyy from this format 2021-04-24
        System.out.println("Today is " + dateToday);

        System.out.println("Our next lesson will be on " +dateToday.plusDays(14));

        LocalDate anotherToday = LocalDate.now();

        System.out.println("Format it like yy/dd/MM  "
                +anotherToday.format(DateTimeFormatter.ofPattern("yy/dd/MM")));
    }
}
