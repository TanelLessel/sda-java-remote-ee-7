package ee.sda.seven.statickeyword;

public class Student {

    // Fields
    private String firstName;
    private String lastName;
    private String id;
    // static field
    // Common state
    public static String faculty = "Computer science";
    private int age;

    // Reading
    public String getFirstName() {
        return firstName;
    }

    // Writing
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public static String getFaculty() {
        return faculty;
    }

    // static method
    // Common behavior
    public static void study(){

        System.out.println("They are all studying.");
    }
}
